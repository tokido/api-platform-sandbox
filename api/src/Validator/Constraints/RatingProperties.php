<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class RatingProperties extends Constraint
{
    public $message = 'The review rating must be an integer between 0 and 5';
}

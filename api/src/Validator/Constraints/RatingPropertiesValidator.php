<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
final class RatingPropertiesValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!is_int($value) || $value < 0 ||  $value > 5) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}

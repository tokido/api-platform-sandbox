<?php

namespace App\Entity;

use App\Entity\UuidTrait;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\JoinColumn;
use App\Validator\Constraints\RatingProperties;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A review of a book.
 *
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get", "delete"}
 * )
 */
class Review
{
    use UuidTrait;
    /**
     * @var int The id of this review.
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $uuid;

    /**
     * @var int The rating of this review (between 0 and 5).
     *
     * @ORM\Column(type="smallint")
     * @RatingProperties
     */
    public $rating;

    /**
     * @var string the body of the review.
     *
     * @ORM\Column(type="text")
     */
    public $body;

    /**
     * @var string The author of the review.
     *
     * @ORM\Column
     */
    public $author;

    /**
     * @var \DateTimeInterface The date of publication of this review.
     *
     * @ORM\Column(type="datetime")
     */
    public $publicationDate;

    /**
     * @var Book The book this review is about.
     *
     * @ORM\ManyToOne(targetEntity="Book", inversedBy="reviews")
     * @JoinColumn(name="book_id", referencedColumnName="uuid")
     */
    public $book;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
    }
    public function getUuid(): ?string
    {
        return $this->uuid;
    }
}

<?php

namespace App\Entity;
use Ramsey\Uuid\Uuid;

trait UuidTrait {

    public function generateUuid()
    {
        return Uuid::uuid4();
    }
}
